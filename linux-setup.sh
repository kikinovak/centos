#!/bin/bash
#
# linux-setup.sh
#
# (c) Niki Kovacs 2020 <info@microlinux.fr>
#
# This script turns a minimal CentOS 7 installation into a more functional base
# system for various server configurations.

# Current directory
CWD=$(pwd)

# Slow things down a bit
SLEEP=1

# Make sure we're running CentOS 7
source /etc/os-release
if [ "${?}" -ne 0 ]
then
  echo
  echo "  Unsupported operating system." >&2
  echo
  exit 1
fi
if [[ "${REDHAT_SUPPORT_PRODUCT} ${REDHAT_SUPPORT_PRODUCT_VERSION}" != "centos 7" ]]
then
  echo
  echo "  Please run this script on CentOS 7 only." >&2
  echo
  exit 1
fi

echo
echo "  ##########################"
echo "  # CentOS 7 configuration #"
echo "  ##########################"
echo
sleep ${SLEEP}

# Enterprise Linux version
VERSION="el7"

# Defined users
USERS="$(awk -F: '$3 > 999 {print $1}' /etc/passwd | sort)"

# Admin user
ADMIN=$(getent passwd 1000 | cut -d: -f 1)

# Remove these packages
CRUFT=$(egrep -v '(^\#)|(^\s+$)' ${CWD}/${VERSION}/yum/useless-packages.txt)

# Install these packages
EXTRA=$(egrep -v '(^\#)|(^\s+$)' ${CWD}/${VERSION}/yum/extra-packages.txt)

# Enhanced base system
BASE=$(egrep -v '(^\#)|(^\s+$)' ${CWD}/${VERSION}/yum/enhanced-base.txt)

# Mirrors
ELREPO="http://mirrors.ircam.org/pub/elrepo/elrepo/${VERSION}/x86_64/RPMS"
DOCKER="https://download.docker.com/linux/centos"
CISOFY="https://packages.cisofy.com"
ICINGA="https://packages.icinga.com"

# Log
LOG="/tmp/$(basename "${0}" .sh).log"
echo > ${LOG}

usage() {
  # Display help message
  echo "  Usage: ${0} OPTION"
  echo
  echo "  CentOS 7 post-install configuration for servers."
  echo
  echo "  Options:"
  echo "    --shell    Configure shell: Bash, Vim, console, etc."
  echo "    --repos    Setup official and third-party repositories."
  echo "    --fresh    Sync repositories and fetch updates."
  echo "    --extra    Install enhanced base system."
  echo "    --strip    Remove unneeded system components."
  echo "    --logs     Enable admin user to access system logs."
  echo "    --ipv4     Disable IPv6 and reconfigure basic services."
  echo "    --sudo     Configure persistent password for sudo."
  echo "    --setup    Perform all of the above in one go."
  echo "    --reset    Revert back to enhanced base system."
  echo
  echo "  Logs are written to ${LOG}."
  echo
}

configure_shell() {
  echo "  === Shell configuration ==="
  echo
  sleep ${SLEEP}
  # Install custom command prompts and a handful of nifty aliases.
  echo "  Configuring Bash shell for user: root"
  cat ${CWD}/${VERSION}/bash/bashrc-root > /root/.bashrc
  sleep ${SLEEP}
  echo "  Configuring Bash shell for future users."
  cat ${CWD}/${VERSION}/bash/bashrc-users > /etc/skel/.bashrc
  sleep ${SLEEP}
  # Existing users might want to use it.
  if [ ! -z "${USERS}" ]
  then
    for USER in ${USERS}
    do
      if [ -d /home/${USER} ]
      then
	echo "  Configuring Bash shell for user: ${USER}"
	cat ${CWD}/${VERSION}/bash/bashrc-users > /home/${USER}/.bashrc
	chown ${USER}:${USER} /home/${USER}/.bashrc
	sleep ${SLEEP}
      fi
    done
  fi
  # Add a handful of nifty system-wide options for Vim.
  echo "  Configuring system-wide options for Vim."
  cat ${CWD}/${VERSION}/vim/vimrc > /etc/vimrc
  sleep ${SLEEP}
  # Set english as main system language.
  echo "  Configuring system locale."
  localectl set-locale LANG=en_US.UTF-8
  sleep ${SLEEP}
  # Set console resolution
  echo "  Configuring console resolution."
  sed -i -e 's/rhgb quiet/nomodeset quiet vga=791/g' /etc/default/grub
  sleep ${SLEEP}
  if [ -f /boot/grub2/grub.cfg ]
  then
    echo "  Updating bootloader on BIOS system."
    grub2-mkconfig -o /boot/grub2/grub.cfg >> ${LOG} 2>&1
  elif [ -f /boot/efi/EFI/centos/grub.cfg ]
  then
    echo "  Updating bootloader on UEFI system."
    grub2-mkconfig -o /boot/efi/EFI/centos/grub.cfg >> ${LOG} 2>&1
  fi
  echo
}

configure_repos() {
  echo "  === Package repository configuration ==="
  echo
  sleep ${SLEEP}
  echo "  Removing existing repositories."
  rm -f /etc/yum.repos.d/*.repo
  rm -f /etc/yum.repos.d/*.repo.rpmsave
  sleep ${SLEEP}
  # Enable [base], [updates] and [extra] repos with a priority of 1.
  echo "  Configuring official package repositories."
  cat ${CWD}/${VERSION}/yum.repos.d/CentOS-Base.repo > /etc/yum.repos.d/CentOS-Base.repo
  yum -y reinstall centos-release >> ${LOG} 2>&1
  # Cleanup.
  for REPOSITORY in CR Debuginfo fasttrack Media Sources Vault x86_64-kernel
  do 
    echo "  Removing repository: ${REPOSITORY}"
    rm -f /etc/yum.repos.d/CentOS-${REPOSITORY}.repo
    sleep ${SLEEP}
  done
  # Enable [sclo] repos with a priority of 1.
  if ! rpm -q centos-release-scl > /dev/null 2>&1
  then
    echo "  Installing repository: Software Collections"
    yum -y install centos-release-scl >> ${LOG} 2>&1
  fi
  echo "  Configuring repository: Software Collections"
  cat ${CWD}/${VERSION}/yum.repos.d/CentOS-SCLo-scl.repo > /etc/yum.repos.d/CentOS-SCLo-scl.repo
  cat ${CWD}/${VERSION}/yum.repos.d/CentOS-SCLo-scl-rh.repo > /etc/yum.repos.d/CentOS-SCLo-scl-rh.repo
  sleep ${SLEEP}
  # Install Yum-Priorities plugin
  if ! rpm -q yum-plugin-priorities > /dev/null 2>&1
  then
    echo "  Installing Yum-Priorities plugin."
    yum -y install yum-plugin-priorities >> ${LOG} 2>&1
  fi
  # Enable [epel] repo with a priority of 10.
  if ! rpm -q epel-release > /dev/null 2>&1
  then
    echo "  Installing repository: EPEL" 
    yum -y install epel-release >> ${LOG} 2>&1
  fi
  echo "  Configuring repository: EPEL" 
  cat ${CWD}/${VERSION}/yum.repos.d/epel.repo > /etc/yum.repos.d/epel.repo
  echo "  Removing repository: EPEL Testing"
  rm -f /etc/yum.repos.d/epel-testing.repo
  sleep ${SLEEP}
  # Configure [elrepo] and [elrepo-kernel] repos without activating them.
  if ! rpm -q elrepo-release > /dev/null 2>&1
  then
    echo "  Installing repository: ELRepo"
    yum -y install elrepo-release >> ${LOG} 2>&1
  fi
  echo "  Configuring repository: ELRepo"
  cat ${CWD}/${VERSION}/yum.repos.d/elrepo.repo > /etc/yum.repos.d/elrepo.repo
  sleep ${SLEEP}
  # Enable [lynis] repo with a priority of 5.
  echo "  Configuring repository: Lynis"
  rpm --import ${CISOFY}/keys/cisofy-software-rpms-public.key >> ${LOG} 2>&1
  cat ${CWD}/${VERSION}/yum.repos.d/lynis.repo > /etc/yum.repos.d/lynis.repo
  sleep ${SLEEP}
  if ! rpm -q icinga-rpm-release > /dev/null 2>&1
  then
    echo "  Installing repository: Icinga"
    yum -y install ${ICINGA}/epel/icinga-rpm-release-7-latest.noarch.rpm >> ${LOG} 2>&1
  fi
  echo "  Configuring repository: Icinga"
  cat ${CWD}/${VERSION}/yum.repos.d/ICINGA-release.repo > /etc/yum.repos.d/ICINGA-release.repo
  rm -f /etc/yum.repos.d/ICINGA-snapshot.repo
  sleep ${SLEEP}
  echo "  Configuring repository: Docker"
  rpm --import ${DOCKER}/gpg >> ${LOG} 2>&1
  cat ${CWD}/${VERSION}/yum.repos.d/docker-ce.repo > /etc/yum.repos.d/docker-ce.repo
  echo
  sleep ${SLEEP}
} 

update_system() {
  echo "  === Update system ==="
  echo
  sleep ${SLEEP}
  if ! rpm -q deltarpm > /dev/null 2>&1
  then
    echo "  Enabling Delta RPM."
    yum -y install deltarpm >> ${LOG} 2>&1
  fi
  # Update system.
  echo "  Performing system update."
  sleep ${SLEEP}
  echo "  This might take a moment..."
  yum -y update >> ${LOG} 2>&1
  echo
}

install_extras() {
  echo "  === Install extra packages ==="
  echo
  sleep ${SLEEP}
  echo "  Fetching missing packages from Core package group." 
  yum -y group mark remove "Core" >> ${LOG} 2>&1
  yum -y group install "Core" >> ${LOG} 2>&1
  echo "  Core package group installed on the system."
  sleep ${SLEEP}
  echo "  Installing Base package group."
  sleep ${SLEEP}
  echo "  This might take a moment..."
  yum -y group mark remove "Base" >> ${LOG} 2>&1
  yum -y group install "Base" >> ${LOG} 2>&1
  echo "  Base package group installed on the system."
  sleep ${SLEEP}
  echo "  Installing some additional packages."
  sleep ${SLEEP}
  for PACKAGE in ${EXTRA}
  do
    if ! rpm -q ${PACKAGE} > /dev/null 2>&1
    then
      echo "  Installing package: ${PACKAGE}"
      yum -y install ${PACKAGE} >> ${LOG} 2>&1
    fi
  done
  echo "  Additional packages installed on the system."
  echo
  sleep ${SLEEP}
}
 
remove_cruft() {
  echo "  === Remove useless packages ==="
  echo
  sleep ${SLEEP}
  echo "  Removing unneeded components from the system."
  sleep ${SLEEP}
  for PACKAGE in ${CRUFT}
  do
    if rpm -q ${PACKAGE} > /dev/null 2>&1
    then
      echo "  Removing package: ${PACKAGE}"
      yum -y remove ${PACKAGE} >> ${LOG} 2>&1
      if [ "${?}" -ne 0 ]
        then
        echo "  Could not remove package ${PACKAGE}." >&2
        exit 1
      fi
    fi
  done
  echo "  Unneeded components removed from the system."
  echo
  sleep ${SLEEP}
}

configure_logs() {
  echo "  === Configure logging ==="
  echo
  sleep ${SLEEP}
  # Admin user can access system logs
  if [ ! -z "${ADMIN}" ]
  then
    if getent group systemd-journal | grep ${ADMIN} > /dev/null 2>&1
    then
      echo "  Admin user ${ADMIN} is already a member of the systemd-journal group."
    else
      echo "  Adding admin user ${ADMIN} to systemd-journal group."
      usermod -a -G systemd-journal ${ADMIN}
    fi
  fi
  echo
  sleep ${SLEEP}
}

disable_ipv6() {
  echo "  === Use IPv4 only ==="
  echo
  sleep ${SLEEP}
  # Disable IPv6
  echo "  Disabling IPv6."
  sleep ${SLEEP}
  cat ${CWD}/${VERSION}/sysctl.d/disable-ipv6.conf > /etc/sysctl.d/disable-ipv6.conf
  sysctl -p --load /etc/sysctl.d/disable-ipv6.conf >> $LOG 2>&1
  # Reconfigure SSH 
  if [ -f /etc/ssh/sshd_config ]
  then
    echo "  Configuring SSH server for IPv4 only."
    sleep ${SLEEP}
    sed -i -e 's/#AddressFamily any/AddressFamily inet/g' /etc/ssh/sshd_config
    sed -i -e 's/#ListenAddress 0.0.0.0/ListenAddress 0.0.0.0/g' /etc/ssh/sshd_config
  fi
  # Reconfigure Postfix
  if [ -f /etc/postfix/main.cf ]
  then
    echo "  Configuring Postfix server for IPv4 only."
    sleep ${SLEEP}
    sed -i -e 's/# Enable IPv4, and IPv6 if supported/# Enable IPv4/g' /etc/postfix/main.cf
    sed -i -e 's/inet_protocols = all/inet_protocols = ipv4/g' /etc/postfix/main.cf
    systemctl restart postfix
  fi
  # Rebuild initrd
  echo "  Rebuilding initial ramdisk."
  dracut -f -v >> $LOG 2>&1
  echo
}

configure_sudo() {
  echo "  === Configure sudo ==="
  echo
  sleep ${SLEEP}
  # Configure persistent password for sudo.
  if grep timestamp_timeout /etc/sudoers > /dev/null 2>&1
  then
    echo "  Persistent password for sudo already configured."
  else
    echo "  Configuring persistent password for sudo."
    echo >> /etc/sudoers
    echo "# Timeout" >> /etc/sudoers
    echo "Defaults timestamp_timeout=-1" >> /etc/sudoers
  fi
  echo
  sleep ${SLEEP}
}

reset_system() {
  echo "  === Restore enhanced base system ==="
  echo
  sleep ${SLEEP}
  # Remove all packages that are not part of the enhanced base system.
  echo "  Stripping system."
  local TMP="/tmp"
  local PKGLIST="${TMP}/pkglist"
  local PKGINFO="${TMP}/pkg_base"
  rpm -qa --queryformat '%{NAME}\n' | sort > ${PKGLIST}
  PACKAGES=$(egrep -v '(^\#)|(^\s+$)' $PKGLIST)
  rm -rf ${PKGLIST} ${PKGINFO}
  mkdir ${PKGINFO}
  unset REMOVE
  echo "  Creating database."
  for PACKAGE in ${BASE}
  do
    touch ${PKGINFO}/${PACKAGE}
  done
  for PACKAGE in ${PACKAGES}
  do
    if [ -r ${PKGINFO}/${PACKAGE} ]
    then
      continue
    else
      REMOVE="${REMOVE} ${PACKAGE}"
    fi
  done
  if [ ! -z "${REMOVE}" ]
  then
    for PACKAGE in ${REMOVE}
    do
      if rpm -q ${PACKAGE} > /dev/null 2>&1
      then
        echo "  Removing package: ${PACKAGE}"
        yum -y remove ${PACKAGE} >> ${LOG} 2>&1
      fi
    done
  fi
  configure_repos
  install_extras
  remove_cruft
  rm -rf ${PKGLIST} ${PKGINFO}
  echo
}

# Make sure the script is being executed with superuser privileges.
if [[ "${UID}" -ne 0 ]]
then
  echo
  echo "Please run with sudo or as root." >&2
  echo
  exit 1
fi

# Check parameters.
if [[ "${#}" -ne 1 ]]
then
  usage
  exit 1
fi
OPTION="${1}"
case "${OPTION}" in
  --shell)
    configure_shell
    ;;
  --repos) 
    configure_repos
    ;;
  --fresh)
    update_system
    ;;
  --extra) 
    install_extras
    ;;
  --strip) 
    remove_cruft
    ;;
  --logs) 
    configure_logs
    ;;
  --ipv4) 
    disable_ipv6
    ;;
  --sudo) 
    configure_sudo
    ;;
  --setup) 
    configure_shell
    configure_repos
    update_system
    install_extras
    remove_cruft
    configure_logs
    disable_ipv6
    configure_sudo
    ;;
  --reset) 
    reset_system
    ;;
  --help) 
    usage
    exit 0
    ;;
  ?*) 
    usage
    exit 1
esac

exit 0
