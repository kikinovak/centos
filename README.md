# Post-installation setup script for CentOS servers 

(c) Niki Kovacs, 2021

This repository provides an "automagic" post-installation setup script for
servers running CentOS 7.

## In a nutshell

Perform the following steps.

  1. Install a minimal CentOS 7 system.

  2. Create a non-`root` user with administrator privileges.

  3. Install Git: `sudo yum install git`

  4. Grab the script: `git clone https://gitlab.com/kikinovak/centos.git`

  5. Change into the new directory: `cd centos`

  6. Run the script: `sudo ./linux-setup.sh --setup`

  7. Grab a cup of coffee while the script does all the work.

  8. Reboot.

## Customizing a CentOS server

Turning a minimal CentOS installation into a functional server always boils
down to a series of more or less time-consuming operations. Your mileage may
vary of course, but here's what I usually do on a fresh CentOS installation:

  * Customize the Bash shell : prompt, aliases, etc.

  * Customize the Vim editor.

  * Setup official and third-party repositories.

  * Install a complete set of command line tools.

  * Remove a handful of unneeded system components.

  * Enable the admin user to access system logs.

  * Disable IPv6 and reconfigure some services accordingly.
  
  * Configure a persistent password for `sudo`.

  * Etc.

The `linux-setup.sh` script performs all of these operations.

Configure Bash and Vim and set a more readable default console resolution:

```
# ./linux-setup.sh --shell
```

Setup official and third-party repositories:

```
# ./linux-setup.sh --repos
```

Enable DeltaRPM and fetch system updates:

```
# ./linux-setup.sh --fresh
```

Install the `Core` and `Base` package groups along with some extra tools:

```
# ./linux-setup.sh --extra
```

Remove a handful of unneeded system components:

```
# ./linux-setup.sh --strip
```

Enable the admin user to access system logs:

```
# ./linux-setup.sh --logs
```

Disable IPv6 and reconfigure basic services accordingly:

```
# ./linux-setup.sh --ipv4
```

Configure password persistence for `sudo`:

```
# ./linux-setup.sh --sudo
```

Perform all of the above in one go:

```
# ./linux-setup.sh --setup
```

Strip packages and revert back to an enhanced base system:

```
# ./linux-setup.sh --reset
```

Display help message:

```
# ./linux-setup.sh --help
```

If you want to know what exactly goes on under the hood, open a second terminal
and view the logs:

```
$ tail -f /tmp/centos-7.8-setup.log
```

